package managedBeans;

import java.util.Date;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import com.advyteam.entities.Candidature;
import com.advyteam.entities.Job;
import com.advyteam.enumerations.EtatCandidature;
import com.advyteam.interfaces.CandidatureServiceRemote;
import com.advyteam.interfaces.JobServiceRemote;

@ManagedBean(name = "postulerJob")
public class PostulerJobBean {

	@ManagedProperty(value = "#{jobManagedBean}")
	private JobManagedBean jobManagedBean;

	@EJB
	CandidatureServiceRemote candidatureServiceRemote;

	private Candidature candidature = new Candidature();

	public String candidater() {

		candidature.setDateCandidature(new Date());
		candidature.setEtatCandidature(EtatCandidature.EN_COURS);
		candidature.setJob(jobManagedBean.getJob());

		// CV hardcoded
		candidature.setCv("CVCVCVCV");

		// Change the id of the employee
		candidatureServiceRemote.ajouterCandidature(candidature, 7);

		return "candidature-success?faces-redirect=true";

	}

	public Candidature getCandidature() {
		return candidature;
	}

	public void setCandidature(Candidature candidature) {
		this.candidature = candidature;
	}

	public JobManagedBean getJobManagedBean() {
		return jobManagedBean;
	}

	public void setJobManagedBean(JobManagedBean jobManagedBean) {
		this.jobManagedBean = jobManagedBean;
	}

	public CandidatureServiceRemote getCandidatureServiceRemote() {
		return candidatureServiceRemote;
	}

	public void setCandidatureServiceRemote(CandidatureServiceRemote candidatureServiceRemote) {
		this.candidatureServiceRemote = candidatureServiceRemote;
	}

}
