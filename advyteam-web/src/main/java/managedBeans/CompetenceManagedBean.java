package managedBeans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import com.advyteam.entities.Competence;
import com.advyteam.interfaces.CompetenceServiceRemote;

@ManagedBean
public class CompetenceManagedBean {

	@EJB
	CompetenceServiceRemote competenceServiceRemote;
	private Competence competence = new Competence();

	public String ajoutCompetence() {
		competenceServiceRemote.addCompetence(competence);
		return "success-ajout-competence?faces-redirect=true";

	}

	public CompetenceServiceRemote getCompetenceServiceRemote() {
		return competenceServiceRemote;
	}

	public void setCompetenceServiceRemote(CompetenceServiceRemote competenceServiceRemote) {
		this.competenceServiceRemote = competenceServiceRemote;
	}

	public Competence getCompetence() {
		return competence;
	}

	public void setCompetence(Competence competence) {
		this.competence = competence;
	}

}
