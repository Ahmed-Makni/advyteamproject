package managedBeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.advyteam.entities.Job;
import com.advyteam.interfaces.JobServiceRemote;

@ManagedBean(name = "jobManagedBean")
@SessionScoped
public class JobManagedBean {

	@EJB
	JobServiceRemote jobServiceRemote;
	public List<Job> jobs = new ArrayList<Job>();

	public Job job = new Job();

	@PostConstruct
	public void x() {

		jobs = jobServiceRemote.allJobs();
		System.out.println(job);

	}

	public String redirectToJobDetail() {
		return "job-details?faces-redirect=true";
	}

	public String savoirPlus(int idJob) throws IOException {
		// job = jobServiceRemote.getJobById(idJob);
		// System.out.println(job.toString());

		// ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		// ec.redirect(ec.getRequestContextPath() + "/job-details.jsf");
		// redirectToJobDetail();
		// return "job-details?faces-redirect=true";

		System.out.println("Sended Id " + idJob);

		this.job = jobServiceRemote.getJobById(idJob);

		return "job-details?faces-redirect=true";
	}

	public void print() {
		System.out.println("Clicked !!");
	}

	public JobServiceRemote getJobServiceRemote() {
		return jobServiceRemote;
	}

	public void setJobServiceRemote(JobServiceRemote jobServiceRemote) {
		this.jobServiceRemote = jobServiceRemote;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

}
