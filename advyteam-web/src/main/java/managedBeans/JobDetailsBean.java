package managedBeans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import com.advyteam.entities.Job;

@ManagedBean(name = "jobDetailsBean")
//@SessionScoped
public class JobDetailsBean {
	@ManagedProperty(value = "#{jobManagedBean}")
	private JobManagedBean jobManagedBean;

	private Job job = new Job();

	@PostConstruct
	public void showJobDetails() {
		this.setJob(jobManagedBean.getJob());
		System.out.println("retreived Id => " + this.job.getId());
		System.out.println(this.job.getId());
	}

	public String postulerJob(int idJob) {

		System.out.println("Job a postuler ==> " + idJob);
		return "postuler?faces-redirect=true";
	}

	public JobManagedBean getJobManagedBean() {
		return jobManagedBean;
	}

	public void setJobManagedBean(JobManagedBean jobManagedBean) {
		this.jobManagedBean = jobManagedBean;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

}
