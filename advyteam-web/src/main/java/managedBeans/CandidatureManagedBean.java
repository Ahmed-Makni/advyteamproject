package managedBeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import com.advyteam.entities.Candidature;
import com.advyteam.interfaces.CandidatureServiceRemote;

@ManagedBean(name = "candidatureManagedBean")
public class CandidatureManagedBean {

	// ** **//

	@EJB
	CandidatureServiceRemote candidatureServiceRemote;
	private List<Candidature> mesCandidatures = new ArrayList<Candidature>();

	@PostConstruct
	public void afficherMesCandidature() {

		// id Employee to be changed !!!!

		this.mesCandidatures = candidatureServiceRemote.mesCandidatures(7);
		System.out.println("mes candidatures size => " + this.mesCandidatures.size());

	}
	
	


	public CandidatureServiceRemote getCandidatureServiceRemote() {
		return candidatureServiceRemote;
	}

	public void setCandidatureServiceRemote(CandidatureServiceRemote candidatureServiceRemote) {
		this.candidatureServiceRemote = candidatureServiceRemote;
	}

	public List<Candidature> getMesCandidatures() {
		return mesCandidatures;
	}

	public void setMesCandidatures(List<Candidature> mesCandidatures) {
		this.mesCandidatures = mesCandidatures;
	}
	
	

}
