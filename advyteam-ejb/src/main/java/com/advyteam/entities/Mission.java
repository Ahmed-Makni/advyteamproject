package com.advyteam.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.advyteam.enumerations.EtatMission;

/**
 * Entity implementation class for Entity: Mission
 *
 */
@Entity

public class Mission implements Serializable {

	/**
	 * 
	 */
	// private static final long serialVersionUID = -558553967080513790L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String titre;
	private Date dateDebut;
	private Date dateFin;
	private String lieu;
	private String pays;
	private String description;
	private String qrCode;
	private float fraisLogement;
	private float fraisVisa;
	private float fraisRestaurant;
	private float fraisTransport;
	private float argentPoche;
	@Enumerated(EnumType.STRING)
	private EtatMission etatMission;

	// prop de navig
	@ManyToOne
	private Employee employee;

	@OneToMany(mappedBy = "mission")
	private List<Frais> frais;

	@OneToMany(mappedBy = "mission")
	private List<DemandeMission> demandeMissions;

	public Mission() {
		super();
	}

	public Mission(int id) {
		this.id = id;
	}

	public Mission(String titre, Date dateDebut, Date dateFin, String lieu, String pays, String description,
			String qrCode, float fraisLogement, float fraisVisa, float fraisRestaurant, float fraisTransport,
			float argentPoche, EtatMission etatMission) {
		super();
		this.titre = titre;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.lieu = lieu;
		this.pays = pays;
		this.description = description;
		this.qrCode = qrCode;
		this.fraisLogement = fraisLogement;
		this.fraisVisa = fraisVisa;
		this.fraisRestaurant = fraisRestaurant;
		this.fraisTransport = fraisTransport;
		this.argentPoche = argentPoche;
		this.etatMission = etatMission;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return this.titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public Date getDateDebut() {
		return this.dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return this.dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public String getLieu() {
		return this.lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public String getPays() {
		return this.pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getQrCode() {
		return this.qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public float getFraisLogement() {
		return this.fraisLogement;
	}

	public void setFraisLogement(float fraisLogement) {
		this.fraisLogement = fraisLogement;
	}

	public float getFraisVisa() {
		return this.fraisVisa;
	}

	public void setFraisVisa(float fraisVisa) {
		this.fraisVisa = fraisVisa;
	}

	public float getFraisRestaurant() {
		return this.fraisRestaurant;
	}

	public void setFraisRestaurant(float fraisRestaurant) {
		this.fraisRestaurant = fraisRestaurant;
	}

	public float getFraisTransport() {
		return this.fraisTransport;
	}

	public void setFraisTransport(float fraisTransport) {
		this.fraisTransport = fraisTransport;
	}

	public float getArgentPoche() {
		return this.argentPoche;
	}

	public void setArgentPoche(float argentPoche) {
		this.argentPoche = argentPoche;
	}

	public EtatMission getEtatMission() {
		return this.etatMission;
	}

	public void setEtatMission(EtatMission etatMission) {
		this.etatMission = etatMission;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<Frais> getFrais() {
		return frais;
	}

	public void setFrais(List<Frais> frais) {
		this.frais = frais;
	}

	public List<DemandeMission> getDemandeMissions() {
		return demandeMissions;
	}

	public void setDemandeMissions(List<DemandeMission> demandeMissions) {
		this.demandeMissions = demandeMissions;
	}

	@Override
	public String toString() {
		return "Mission [id=" + id + ", titre=" + titre + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin
				+ ", lieu=" + lieu + ", pays=" + pays + ", description=" + description + ", qrCode=" + qrCode
				+ ", fraisLogement=" + fraisLogement + ", fraisVisa=" + fraisVisa + ", fraisRestaurant="
				+ fraisRestaurant + ", fraisTransport=" + fraisTransport + ", argentPoche=" + argentPoche
				+ ", etatMission=" + etatMission + "]";
	}

}
