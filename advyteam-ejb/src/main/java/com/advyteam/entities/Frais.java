package com.advyteam.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.advyteam.enumerations.CategorieFrais;

/**
 * Entity implementation class for Entity: Frais
 *
 */
@Entity
public class Frais implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Enumerated(EnumType.STRING)
	private CategorieFrais categorie;
	private float montant;
	private String image;
	// private static final long serialVersionUID = -558553967080513790L;

	// prop de navig
	@ManyToOne
	private Mission mission;

	public Frais() {
		super();
	}

	public Frais(CategorieFrais categorie, float montant, String image) {
		super();
		this.categorie = categorie;
		this.montant = montant;
		this.image = image;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CategorieFrais getCategorie() {
		return this.categorie;
	}

	public void setCategorie(CategorieFrais categorie) {
		this.categorie = categorie;
	}

	public float getMontant() {
		return this.montant;
	}

	public void setMontant(float montant) {
		this.montant = montant;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Mission getMission() {
		return mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}

}
