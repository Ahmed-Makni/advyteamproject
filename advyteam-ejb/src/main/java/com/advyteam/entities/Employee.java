package com.advyteam.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.advyteam.enumerations.Role;

@Entity
public class Employee implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Enumerated(EnumType.STRING)
	private Role role;

	// prop de navig (EMNA)
	@OneToMany(mappedBy = "employee")
	private List<Candidature> candidatures;

	@OneToMany
	private List<Competence> competenceEmployes;

	// prop de navig (AMAL)
	@OneToMany(mappedBy = "employee")
	private List<Mission> missions;

	// prop de navig (AMAL)
	@OneToMany(mappedBy = "employee")
	private List<DemandeMission> demandeMissions;

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	// TO DELETE!!
	public Employee(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Candidature> getCandidatures() {
		return candidatures;
	}

	public void setCandidatures(List<Candidature> candidatures) {
		this.candidatures = candidatures;
	}

	public List<Mission> getMissions() {
		return missions;
	}

	public void setMissions(List<Mission> missions) {
		this.missions = missions;
	}

	public List<Competence> getCompetenceEmployes() {
		return competenceEmployes;
	}

	public void setCompetenceEmployes(List<Competence> competenceEmployes) {
		this.competenceEmployes = competenceEmployes;
	}

}
