package com.advyteam.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.advyteam.enumerations.EtatCandidature;

/**
 * Entity implementation class for Entity: Candidature
 *
 */
@Entity
public class Candidature implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Date dateCandidature;
	private String raison;
	private String linkedin;
	private String siteWeb;

	private String telephone;
	private String email;
	private String cv;
	@Enumerated(EnumType.STRING)
	private EtatCandidature etatCandidature;

	// prop de navig
	@ManyToOne
	private Employee employee;

	@ManyToOne
	private Job job;

	public Candidature() {
		super();
	}

	public Candidature(Date dateCandidature, String raison, String linkedin, String siteWeb, String telephone,
			String email, String cv, EtatCandidature etatCandidature) {
		super();
		this.dateCandidature = dateCandidature;
		this.raison = raison;
		this.linkedin = linkedin;
		this.siteWeb = siteWeb;

		this.telephone = telephone;
		this.email = email;
		this.cv = cv;
		this.etatCandidature = etatCandidature;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateCandidature() {
		return this.dateCandidature;
	}

	public void setDateCandidature(Date dateCandidature) {
		this.dateCandidature = dateCandidature;
	}

	public String getRaison() {
		return this.raison;
	}

	public void setRaison(String raison) {
		this.raison = raison;
	}

	public String getLinkedin() {
		return this.linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	public String getSiteWeb() {
		return this.siteWeb;
	}

	public void setSiteWeb(String siteWeb) {
		this.siteWeb = siteWeb;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCv() {
		return this.cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public EtatCandidature getEtatCandidature() {
		return etatCandidature;
	}

	public void setEtatCandidature(EtatCandidature etatCandidature) {
		this.etatCandidature = etatCandidature;
	}

	@Override
	public String toString() {
		return "Candidature [id=" + id + ", dateCandidature=" + dateCandidature + ", raison=" + raison + ", linkedin="
				+ linkedin + ", siteWeb=" + siteWeb + ", telephone=" + telephone + ", email=" + email + ", cv=" + cv
				+ ", etatCandidature=" + etatCandidature + "]";
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

}
