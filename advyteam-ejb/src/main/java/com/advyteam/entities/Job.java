package com.advyteam.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.advyteam.enumerations.Contrat;

/**
 * Entity implementation class for Entity: Job
 *
 */
@Entity
public class Job implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String titre;
	private String descriptionPoste;
	private String profilRecherche;
	@Enumerated(EnumType.STRING)
	private Contrat typeContrat;
	private String lieu;
	private String formation;
	private String experience;
	private Date datePublication;
	private String image;

	@OneToMany(fetch = FetchType.EAGER)
	private List<Competence> competences;

	@OneToMany(mappedBy = "job")
	private List<Candidature> candidatures;

	// private static final long serialVersionUID = -558553967080513790L;

	public Job() {
		super();
	}

	public Job(int id) {
		this.id = id;
	}

	public List<Competence> getCompetences() {
		return competences;
	}

	public void setCompetences(List<Competence> competences) {
		this.competences = competences;
	}

	public Job(String titre, String descriptionPoste, String profilRecherche, Contrat typeContrat, String lieu,
			String formation, String experience, Date datePublication, String image, String competence) {
		super();
		this.titre = titre;
		this.descriptionPoste = descriptionPoste;
		this.profilRecherche = profilRecherche;
		this.typeContrat = typeContrat;
		this.lieu = lieu;
		this.formation = formation;
		this.experience = experience;
		this.datePublication = datePublication;
		this.image = image;

	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitre() {
		return this.titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescriptionPoste() {
		return this.descriptionPoste;
	}

	public void setDescriptionPoste(String descriptionPoste) {
		this.descriptionPoste = descriptionPoste;
	}

	public String getProfilRecherche() {
		return this.profilRecherche;
	}

	public void setProfilRecherche(String profilRecherche) {
		this.profilRecherche = profilRecherche;
	}

	public Contrat getTypeContrat() {
		return this.typeContrat;
	}

	public void setTypeContrat(Contrat typeContrat) {
		this.typeContrat = typeContrat;
	}

	public String getLieu() {
		return this.lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public String getFormation() {
		return this.formation;
	}

	public void setFormation(String formation) {
		this.formation = formation;
	}

	public String getExperience() {
		return this.experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public Date getDatePublication() {
		return this.datePublication;
	}

	public void setDatePublication(Date datePublication) {
		this.datePublication = datePublication;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "Job [id=" + id + ", titre=" + titre + ", descriptionPoste=" + descriptionPoste + ", profilRecherche="
				+ profilRecherche + ", typeContrat=" + typeContrat + ", lieu=" + lieu + ", formation=" + formation
				+ ", experience=" + experience + ", datePublication=" + datePublication + ", image=" + image + "]";
	}

	public List<Candidature> getCandidatures() {
		return candidatures;
	}

	public void setCandidatures(List<Candidature> candidatures) {
		this.candidatures = candidatures;
	}

}
