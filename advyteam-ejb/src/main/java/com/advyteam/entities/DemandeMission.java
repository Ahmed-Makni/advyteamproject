package com.advyteam.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.advyteam.enumerations.EtatDemandeMission;

/**
 * Entity implementation class for Entity: DemandeMission
 *
 */
@Entity

public class DemandeMission implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Date dateDemande;
	@Enumerated(EnumType.STRING)
	private EtatDemandeMission etatDemandeMission;
	// private static final long serialVersionUID = -558553967080513790L;

	// prop de navig

	@ManyToOne
	private Employee employee;

	@ManyToOne
	private Mission mission;

	public DemandeMission() {
		super();
	}

	public DemandeMission(Date dateDemande, EtatDemandeMission etatDemandeMission) {
		super();
		this.dateDemande = dateDemande;
		this.etatDemandeMission = etatDemandeMission;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateDemande() {
		return this.dateDemande;
	}

	public void setDateDemande(Date dateDemande) {
		this.dateDemande = dateDemande;
	}

	public EtatDemandeMission getEtatDemandeMission() {
		return this.etatDemandeMission;
	}

	public void setEtatDemandeMission(EtatDemandeMission etatDemandeMission) {
		this.etatDemandeMission = etatDemandeMission;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Mission getMission() {
		return mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}

	@Override
	public String toString() {
		return "DemandeMission [id=" + id + ", dateDemande=" + dateDemande + ", etatDemandeMission="
				+ etatDemandeMission + ", employee=" + employee + ", mission=" + mission + "]";
	}

}
