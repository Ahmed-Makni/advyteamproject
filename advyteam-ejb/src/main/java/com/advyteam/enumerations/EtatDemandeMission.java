package com.advyteam.enumerations;

public enum EtatDemandeMission {
	REFUSEE, EN_COURS, ACCEPTEE
}
