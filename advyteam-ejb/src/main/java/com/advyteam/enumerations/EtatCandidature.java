package com.advyteam.enumerations;

public enum EtatCandidature {
	EN_COURS, REUFSEE_SCORE, REFUSEE_COMPETENCES, ACCEPTEE
}
