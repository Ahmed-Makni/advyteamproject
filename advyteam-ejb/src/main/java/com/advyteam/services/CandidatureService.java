package com.advyteam.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.advyteam.entities.Candidature;
import com.advyteam.entities.Employee;
import com.advyteam.interfaces.CandidatureServiceLocal;
import com.advyteam.interfaces.CandidatureServiceRemote;

/**
 * Session Bean implementation class CandidatureService
 */
@Stateless
@LocalBean
public class CandidatureService implements CandidatureServiceRemote, CandidatureServiceLocal {

	@PersistenceContext
	EntityManager em;

	public CandidatureService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void ajouterCandidature(Candidature candidature, int idEmployee) {
		candidature.setEmployee(em.find(Employee.class, idEmployee));
		em.persist(candidature);

	}

	@Override
	public List<Candidature> mesCandidatures(int idEmployee) {
		Query query = em.createQuery("select c from Candidature c where c.employee=:l");
		query.setParameter("l", em.find(Employee.class, idEmployee));
		List<Candidature> results = query.getResultList();
		return results;
	}

}
