package com.advyteam.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.advyteam.entities.Competence;
import com.advyteam.entities.Job;
import com.advyteam.interfaces.JobServiceLocal;
import com.advyteam.interfaces.JobServiceRemote;

/**
 * Session Bean implementation class JobService
 */
@Stateless
@LocalBean
public class JobService implements JobServiceRemote, JobServiceLocal {

	@PersistenceContext
	EntityManager em;

	public JobService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void ajouterJob(Job job) {
		em.persist(job);

	}

	@Override
	public void modifierJob(Job job) {
		em.merge(job);

	}

	@Override
	public void supprimerJob(int idJob) {
		em.remove(em.find(Job.class, idJob));

	}

	@Override
	public List<Job> allJobs() {
		Query query = em.createQuery("select j from Job j");
		List<Job> results = query.getResultList();
		return results;
	}

	@Override
	public Job getJobById(int idJob) {
		return em.find(Job.class, idJob);
	}

	@Override
	public double scoreTotaleDesCompetenceParJob(Job job) {
		// TODO Auto-generated method stub
		// job=em.find(Job.class, job.getId());

		List<Competence> competences = job.getCompetences();
		double somme = 0;
		for (Competence competence : competences) {
			somme += competence.getScore();
		}

		return somme;
	}

	@Override
	public double scoreJobParCompetence(Job j, String c) {
		// TODO Auto-generated method stub
		List<Competence> competences = j.getCompetences();
		for (Competence competence : competences) {
			if (competence.getNom().equals(c)) {
				return competence.getScore();
			}
		}

		return 0;
	}
}
