package com.advyteam.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.advyteam.entities.Competence;
import com.advyteam.entities.Employee;
import com.advyteam.interfaces.EmployeeServiceLocal;
import com.advyteam.interfaces.EmployeeServiceRemote;

/**
 * Session Bean implementation class EmployeeService
 */
@Stateless
@LocalBean
public class EmployeeService implements EmployeeServiceRemote, EmployeeServiceLocal {

	@PersistenceContext
	EntityManager em;

	public EmployeeService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addEmployee(Employee employee) {
		em.persist(employee);

	}

	@Override
	public void deleteEmployee(Employee employee) {
		em.remove(employee);

	}

	@Override
	public void updateEmployee(Employee employee) {
		em.merge(employee);

	}

	@Override
	public Employee getEmployeeById(int idemployee) {
		return em.find(Employee.class, idemployee);
	}

	@Override
	public double scoreTotaleDesCompetencesParEmploye(Employee employee) {
		// TODO Auto-generated method stub
		List<Competence> competences = employee.getCompetenceEmployes();
		double somme = 0;
		for (Competence competence : competences) {
			somme += competence.getScore();
		}

		return somme;
	}

	@Override
	public double scoreEmployeParCompetence(Employee e, String c) {
		// TODO Auto-generated method stub
		List<Competence> competences = e.getCompetenceEmployes();
		for (Competence competence : competences) {
			if (competence.getNom().equals(c)) {
				return competence.getScore();
			}
		}

		return 0;
	}

	@Override
	public double scoreEmployerParCompetences(Employee e, List<Competence> competences) {
		// TODO Auto-generated method stub

		List<Competence> competences2 = e.getCompetenceEmployes();

		double somme = 0;

		for (Competence comEmploye : competences2) {
			for (Competence c : competences2) {

				if (comEmploye.getNom().equals(c.getNom())) {
					somme += comEmploye.getScore();
				}
			}
			return somme;
		}

		return 0;
	}

}
