package com.advyteam.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.advyteam.entities.Competence;
import com.advyteam.entities.EmpComp;
import com.advyteam.interfaces.EmpCompServiceLocal;
import com.advyteam.interfaces.EmpCompServiceRemote;

/**
 * Session Bean implementation class EmpCompService
 */
@Stateless
@LocalBean
public class EmpCompService implements EmpCompServiceRemote, EmpCompServiceLocal {

	@PersistenceContext
	EntityManager em;

	public EmpCompService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void affectCompetenceToEmployee(EmpComp employeeCompetence) {
		if (checkIfEmployeeHasCompetence(employeeCompetence)) {
			System.out.println("Employee already have this competency !");
		} else {
			em.persist(employeeCompetence);
		}
	}

	@Override
	public boolean checkIfEmployeeHasCompetence(EmpComp employeeCompetence) {
		Query query = em.createQuery("select ec from EmpComp ec where ec.employeeId=:l and ec.competenceId=:p");
		query.setParameter("l", employeeCompetence.getEmployeeId()).setParameter("p",
				employeeCompetence.getCompetenceId());

		if (query.getResultList().size() > 0) {
			return true;
		}

		return false;
	}

	@Override
	public void updateScoreCompetence(EmpComp employeeCompetence, float score) {
		Query query = em.createQuery("select ec from EmpComp ec where ec.employeeId=:l and ec.competenceId=:p");
		query.setParameter("l", employeeCompetence.getEmployeeId()).setParameter("p",
				employeeCompetence.getCompetenceId());

		EmpComp empComp = (EmpComp) query.getResultList().get(0);
		empComp.setScore(score);
		em.merge(empComp);
		System.out.println("Score Updated!");

	}

	@Override
	public List<Competence> getCompetencesByEmployee(int idEmployee) {
		Query query = em.createQuery("select ec from EmpComp ec where ec.employeeId=:l");
		query.setParameter("l", idEmployee);

		List<EmpComp> results = query.getResultList();

		List<Competence> competences = new ArrayList<Competence>();

		for (EmpComp item : results) {
			competences.add(em.find(Competence.class, item.getCompetenceId()));
		}

		return competences;
	}

}
