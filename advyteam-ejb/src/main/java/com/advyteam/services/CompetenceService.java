package com.advyteam.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.advyteam.entities.Competence;
import com.advyteam.interfaces.CompetenceServiceLocal;
import com.advyteam.interfaces.CompetenceServiceRemote;

/**
 * Session Bean implementation class CompetenceService
 */
@Stateless
@LocalBean
public class CompetenceService implements CompetenceServiceRemote, CompetenceServiceLocal {

	@PersistenceContext
	EntityManager em;

	public CompetenceService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addCompetence(Competence competence) {
		em.persist(competence);

	}

	@Override
	public void deleteCompetence(Competence competence) {
		em.remove(competence);

	}

	@Override
	public Competence getCompetenceById(int id) {

		TypedQuery<Competence> query = em.createQuery("SELECT c FROM Competence c WHERE c.id=:cmp", Competence.class);

		query.setParameter("cmp", id);
		List<Competence> results = query.getResultList();

		return results.get(0);

	}

	

}
