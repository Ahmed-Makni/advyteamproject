package com.advyteam.interfaces;

import java.util.List;

import javax.ejb.Remote;

import com.advyteam.entities.Candidature;

@Remote
public interface CandidatureServiceRemote {

	public void ajouterCandidature(Candidature candidature,int idEmployee);

	public List<Candidature> mesCandidatures(int idEmployee);
	
}
