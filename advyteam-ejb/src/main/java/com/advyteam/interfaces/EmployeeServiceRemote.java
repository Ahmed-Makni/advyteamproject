package com.advyteam.interfaces;

import java.util.List;

import javax.ejb.Remote;

import com.advyteam.entities.Competence;
import com.advyteam.entities.Employee;

@Remote
public interface EmployeeServiceRemote {
	public void addEmployee(Employee employee);

	public void updateEmployee(Employee employee);

	public void deleteEmployee(Employee employee);

	public Employee getEmployeeById(int idemployee);

	public double scoreTotaleDesCompetencesParEmploye(Employee employee);

	public double scoreEmployeParCompetence(Employee e, String c);

	public double scoreEmployerParCompetences(Employee e, List<Competence> competences);
}
