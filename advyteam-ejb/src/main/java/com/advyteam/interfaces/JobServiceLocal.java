package com.advyteam.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.advyteam.entities.Job;

@Local
public interface JobServiceLocal {
	public void ajouterJob(Job job);

	public void modifierJob(Job job);

	public void supprimerJob(int idJob);

	public List<Job> allJobs();

	public Job getJobById(int idJob);

	public double scoreTotaleDesCompetenceParJob(Job job);

	public double scoreJobParCompetence(Job j, String c);

}
