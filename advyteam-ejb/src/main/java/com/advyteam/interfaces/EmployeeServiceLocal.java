package com.advyteam.interfaces;

import javax.ejb.Local;

import com.advyteam.entities.Employee;

@Local
public interface EmployeeServiceLocal {

	public void addEmployee(Employee employee);

	public void updateEmployee(Employee employee);

	public void deleteEmployee(Employee employee);

	public Employee getEmployeeById(int idemployee);

	public double scoreTotaleDesCompetencesParEmploye(Employee employee);

	public double scoreEmployeParCompetence(Employee e, String c);

}
