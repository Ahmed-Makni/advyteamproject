package com.advyteam.interfaces;

import java.util.List;

import javax.ejb.Remote;

import com.advyteam.entities.Job;

@Remote
public interface JobServiceRemote {
	public void ajouterJob(Job job);

	public void modifierJob(Job job);

	public void supprimerJob(int idJob);

	public List<Job> allJobs();

	public Job getJobById(int idJob);

	public double scoreTotaleDesCompetenceParJob(Job job);

	public double scoreJobParCompetence(Job j, String c);

}
