package com.advyteam.interfaces;

import java.util.List;

import javax.ejb.Remote;

import com.advyteam.entities.Competence;
import com.advyteam.entities.EmpComp;

@Remote
public interface EmpCompServiceRemote {
	public void affectCompetenceToEmployee(EmpComp employeeCompetence);
	public boolean checkIfEmployeeHasCompetence(EmpComp employeeCompetence) ;
	public void updateScoreCompetence(EmpComp employeeCompetence,float score);
	public List<Competence> getCompetencesByEmployee(int idEmployee);
}
