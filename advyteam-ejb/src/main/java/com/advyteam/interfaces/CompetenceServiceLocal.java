package com.advyteam.interfaces;

import javax.ejb.Local;

import com.advyteam.entities.Competence;

@Local
public interface CompetenceServiceLocal {
	public void addCompetence(Competence competence);
	
	public void deleteCompetence(Competence competence);
	public Competence getCompetenceById(int id);
	
}
