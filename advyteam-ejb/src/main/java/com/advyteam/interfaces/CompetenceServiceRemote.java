package com.advyteam.interfaces;

import javax.ejb.Remote;

import com.advyteam.entities.Competence;

@Remote
public interface CompetenceServiceRemote {

	public void addCompetence(Competence competence);

	public void deleteCompetence(Competence competence);

	public Competence getCompetenceById(int id);

	

}
