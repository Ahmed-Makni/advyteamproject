package com.advyteam.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.advyteam.entities.Candidature;

@Local
public interface CandidatureServiceLocal {
	public void ajouterCandidature(Candidature candidature,int idEmployee);
	public List<Candidature> mesCandidatures(int idEmployee);

}
